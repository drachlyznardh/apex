
SETUPFILE=setup.py
SETUP=python3 $(SETUPFILE)

NAME=$(shell $(SETUP) --name)
VERSION=$(shell $(SETUP) --version)

SRCDIR=src
RESDIR=res
RPMDIR=rpm

SRCS=$(shell test -d $(SRCDIR) && find $(SRCDIR) -type f)
RESS=$(shell test -d $(RESDIR) && find $(RESDIR) -type f)
RPMFILE=$(RPMDIR)/$(NAME)-$(VERSION)-1.x86_64.rpm

all: package

buildrpm:
	@$(SETUP) sources
	@$(SETUP) bdist_rpm --force-arch $$( uname -m )

$(RPMDIR):
	@mkdir -p $@

$(RPMFILE): $(RPMDIR) Makefile $(SETUPFILE) README.md $(SRCS) $(RESS)
	@vase pybuild . $(RPMDIR)

package: $(RPMFILE)

test baretest: %: $(RPMFILE)
	@vase $@ $(RPMDIR) .

clean:
	@$(SETUP) clean
	@rm -rf $(RPMDIR)

.PHONY: all buildrpm package test baretest clean

