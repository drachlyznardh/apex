#!/usr/bin/env python3

from setuptools import setup, find_packages
from distutils.cmd import Command
import os, os.path

name='apex'
version='0.0.4+1'
srcdir='src'
obsdir='obs'

here=os.path.dirname(__file__)

with open(os.path.join(here, 'README.md'), 'rt') as ifd:
	README = ifd.read()

class SourcesCMD(Command):

	description='''Build the sources for this application'''
	user_options=[]
	def initialize_options(self): pass
	def finalize_options(self): pass
	def run(self):

		basedir = os.path.join(obsdir, name)
		if not os.path.exists(basedir): os.makedirs(basedir)
		with open(os.path.join(basedir, 'VERSION'), 'wt') as ofd:
			print(version, file=ofd)
		with open(os.path.join(here, 'LICENSE'), 'rt') as ifd:
			with open(os.path.join(basedir, 'LICENSE'), 'wt') as ofd:
				print(ifd.read(), file=ofd)

		from distutils.file_util import copy_file

		from pyarmor.pyarmor import main as runPyArmor
		runPyArmor('obfuscate --output {} {}/{}/__init__.py'.format(basedir,srcdir,name).split())

setup(
	name=name,
	version=version,
	author='Ivan Simonini',
	author_email='ivan.simonini@roundhousecode.com',
	description='Another Published EXample',
	long_description=README,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/drachlyznardh/apex',
	package_dir={'':obsdir},
	packages=find_packages(where=obsdir),
	package_data={'': ['VERSION', 'LICENSE',
		'*.lic', '*.key', '*.so', '*.dll']},
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"Operating System :: OS Independent",
	],
	entry_points={'console_scripts':['apex=apex:main']},
	cmdclass={'sources': SourcesCMD},
	setup_requires=['pyarmor'],
)

