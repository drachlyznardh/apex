
import sys, argparse, pkg_resources

with open(pkg_resources.resource_filename(__name__, 'VERSION')) as ifd:
	version = ifd.read().strip()

def main(inArgs=sys.argv[1:]):

	parser = argparse.ArgumentParser(prog=__name__)
	parser.add_argument('-v', '--version', action='version', version='{} v{}'.format(__name__.capitalize(), version))
	parser.add_argument('--machine-version', action='version', version='{}'.format(version), help='Show plain version number, machine-readable')
	parser.add_argument('-l', '--license', action='store_true', default=False, help='Show license')

	args = parser.parse_args(inArgs)

	if args.license:
		with open(pkg_resources.resource_filename(__name__, 'LICENSE')) as ifd: print(ifd.read().strip())
		sys.exit(0)

	# print(args)

